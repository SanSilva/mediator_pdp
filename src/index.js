import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

import Button from "react-bootstrap/Button";

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

class Mediator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Components: [],
    };
    this.aceso = false;
  }
  createLigths(lights) {
    var newComponents = [];
    for (let i = 0; i < lights.length; i++) {
      newComponents.push(lights[i]);
    }
    this.setState({ Components: newComponents });
  }
  execComandos(comandos) {
    console.log("comandos", comandos);

    var newComponents = [...this.state.Components];

    for (let i = 0; i < newComponents.length; i++) {
      let { aceso } = newComponents[i];
      let comando = comandos[i] ? true : false;

      if (aceso !== comando) {
        newComponents[i].aceso = !newComponents[i].aceso;
      }
    }
    sleep(1000);
    console.log("Alterando view");
    this.setState({ Components: newComponents });
  }
}

class Ligth extends Mediator {
  constructor(props) {
    super(props);
    this.comandos = [];
  }
  gerarComando(index) {
    let comandos = [];
    for (let i = 0; i < index; i++) {
      comandos.push(Math.round(Math.random()));
    }
    this.comandos = comandos;
  }
  render() {
    if (this.props.power) {
      return <div className="aceso">aceso</div>;
    } else {
      return <div className="apagado">apagado</div>;
    }
  }
}

class Show extends Mediator {
  render() {
    return this.props.Components.map((component, index) => (
      <Ligth key={index} power={component.aceso} />
    ));
  }
}

class Display extends Mediator {
  handleClickGerar(i) {
    let components = [];
    for (let i = 0; i < 10; i++) {
      components.push(new Ligth());
    }
    this.createLigths(components);
  }
  handleClickRodar(i) {
    let newComponent = [...this.state.Components];
    i = getRandomInt(0, 4);
    newComponent[i].gerarComando(this.state.Components.length);
    console.log("Comando enviado por: ", i);
    this.execComandos(newComponent[i].comandos);
  }
  render() {
    return (
      <div className="mainly">
        <Show Components={this.state.Components} />
        <Button className="primary" onClick={(i) => this.handleClickGerar(i)}>
          Gerar
        </Button>
        <Button className="primary" onClick={(i) => this.handleClickRodar(i)}>
          Rodar
        </Button>
      </div>
    );
  }
}

ReactDOM.render(<Display />, document.getElementById("root"));
